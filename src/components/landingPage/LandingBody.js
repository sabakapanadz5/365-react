import "./LandingBody.css";
const LandingBody = () => {
  return (
    <div className="landingBody__container">
      <img
        className="landing_img"
        src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
      ></img>

      <h4 style={{ paddingTop: "20px" }} className="landingBody__text">
        WE GOT YOUR SUPPLY CHAIN COVERED
      </h4>
      <h4 className="landingBody__text">365 DAYS A YEAR!</h4>
    </div>
  );
};
export default LandingBody;
