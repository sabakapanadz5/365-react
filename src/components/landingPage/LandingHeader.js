import "./LandingHeader.css";
import facebook from "../../media/123123.png";
import { useHistory } from "react-router-dom";
const LandingHeader = () => {
  const history = useHistory();
  const toLogin = () => {
    history.push("/signin");
  };
  const tocatalog = () => {
    history.push("/signup");
  };
  return (
    <div className="landing__header">
      <div>
        <img
          className="landing__logo"
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png"
        ></img>
      </div>
      <div>
        <ul className="landing__list">
          <li className="landing__item landing__item--resp">ABOUT</li>
          <li className="landing__item landing__item--resp">CATALOG</li>
          <li className="landing__item landing__item--resp">PRICING</li>
          <li className="landing__item landing__item--resp">SUPPLIERS</li>
          <li className="landing__item landing__item--resp">HELP CENTER</li>
          <li className="landing__item landing__item--resp">BLOG</li>
          <li onClick={tocatalog} className="landing__item landing__item--border">SIGN UP NOW</li>
          <li onClick={toLogin} className="landing__item landing__item--border">
            LOGIN
          </li>
          <li className="landing__item facebook">
            <img className="facebook" src={facebook} />
          </li>
        </ul>
      </div>
    </div>
  );
};
export default LandingHeader;
