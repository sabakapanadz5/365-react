import Button from "../../components/Common/Button";
import { useHistory } from "react-router-dom";
import "./Landing.css";
import LandingHeader from "./LandingHeader";
import LandingBody from "./LandingBody";

const Landing = () => {
  const history = useHistory();

  const tocatalog = () => {
    history.push("/signup");
  };
  return (
    <div className="landing-container">
      <div className="landing-main">
        <LandingHeader />
        <LandingBody />
        <Button handleClick={tocatalog} signUp title="Sign Up Now" />
      </div>
    </div>
  );
};
export default Landing;
