import "./SidebarNav.css";
import dropshiplogo from "../../media/dropship-logo.png";
import profileimg from "../../media/profile-example.jpg";
import dashboardlogo from "../../media/dashboard.svg";
import bars from "../../media/bars.svg";
import inventorylogo from "../../media/inventory.svg";
import cartlogo from "../../media/cart.svg";
import orderslogo from "../../media/orders.svg";
import transactions from "../../media/transactions.svg";

import { Link } from "react-router-dom";

const SidebarNav = () => {
  return (
    <div className="sidebar__nav">
      <ul className="sidebar__list">
        <li className="sidebar__item sidebar__item--big">
        <Link to="/dropship">
          <a>
            <img src={dropshiplogo} alt="" />
          </a>
          </Link>
        </li>
        <li className="sidebar__item sidebar__item--big">
        <Link to="/profile">
          <a>
            <img className="sidebar__item--radius " src={profileimg} alt="" />
          </a>
        </Link>
        </li>
        <li className="sidebar__item">
        <Link to="/dashboard">
          <a>
            <img src={dashboardlogo} alt="" />
          </a>
          </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/catalog">
            <a>
              <img src={bars} alt="" />
            </a>
          </Link>
        </li>
        <li className="sidebar__item">
        <Link to="/inventory">
          <a>
            <img src={inventorylogo} alt="" />
          </a>
          </Link>
        </li>
        <li className="sidebar__item">
        <Link to="/cart">
          <a>
            <img src={cartlogo} alt="" />
          </a>
        </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/transactions">
            <a>
              <img src={transactions} alt="" />
            </a>
          </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/orders">
            <a>
              <img src={orderslogo} alt="" />
            </a>
          </Link>
        </li>
      </ul>
    </div>
  );
};
export default SidebarNav;
