import "./Aside.css";
import SidebarNav from "./SidebarNav";

const Aside = () => {
  return (
    <aside className="sidebar">
      <SidebarNav />
    </aside>
  );
};
export default Aside;
