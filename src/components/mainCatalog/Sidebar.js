import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { changeCategoryValueAction } from "../../Reducers/ProductsReducer/ProductsAction";
import "./Sidebar.css";
const Sidebar = ({ setcategoryValue, categories, title, light }) => {
  const history = useHistory();
  const dispach = useDispatch();

  const categoryChangeHandler = (event) => {
    dispach(changeCategoryValueAction(event.target.value));
    history.push(`/catalog/${event.target.value}`);
  };
  return (
    <div className="sidebar__wrapper">
      <select
        onChange={categoryChangeHandler}
        className={light ? "sidebar_header--light" : "sidebar_header"}
      >
        <option value="default">{title}</option>
        {categories &&
          categories.map((item) => {
            return <option key={categories.indexOf(item)} value={item}>{item}</option>;
          })}
      </select>
    </div>
  );
};
export default Sidebar;
