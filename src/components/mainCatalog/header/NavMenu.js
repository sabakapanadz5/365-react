import "./NavMenu.css";
import questionLogo from "../../../media/question1.png";
import Button from "../../Common/Button";
import SearchIcon from "@material-ui/icons/Search";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import {
  changeSearchTypeAction,
  getProductsAction,
} from "../../../Reducers/ProductsReducer/ProductsAction";
import sortProducts from "../../../sortProducts";
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/core";
import { Drawer } from "@material-ui/core";
import AddBoxIcon from "@material-ui/icons/AddBox";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import SidebarNav from "../../sidebar/SidebarNav";
import { editModalOpenAction } from "../../../Reducers/ModalReducer/ModalAction";

const styles = makeStyles((theme) => ({
  root: {
    cursor: "pointer",
    color: "#61d5df",
    fontSize: "xx-large",
    margin: "10px",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

const NavMenu = ({ handleClearAll, handleSelectAll, idArray }) => {
  const user = JSON.parse(localStorage.getItem("user")) || {}
  const classes = styles();
  // const idArr = useSelector((state) => state.selectors.idArray);
  const searchType = useSelector((state) => state.products.searchInput);
  const dispach = useDispatch();
  const productsList = useSelector((state) => state.products.productsList);
  const sorttType = useSelector((state) => state.products.sortType);
  const categoryValue = useSelector((state) => state.products.categoryValue);
  const history = useHistory();
  const [drawerOpen, setDrawerOpen] = useState(false);

  const handleSearchClick = () => {
    const products = JSON.parse(localStorage.getItem("products"));
    let filtered = products.filter((products) => {
      return (
        products.title.toLowerCase().includes(searchType.toLowerCase()) &&
        (categoryValue !== "default"
          ? products.category.toLowerCase() === categoryValue.toLowerCase()
          : true)
      );
    });
    dispach(getProductsAction(filtered));
    sortProducts(sorttType, filtered);
  };

  const Logout = () => {
    localStorage.clear();
    history.push("/");
  };
  const searchInputChanged = (e) => {
    dispach(changeSearchTypeAction(e.target.value));
  };
  const handleEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      handleSearchClick(e.target.value);
      dispach(changeSearchTypeAction(""));
    }
  };
  const handleOpen = () =>{
    dispach(editModalOpenAction(true))
  }
  return (
    <div className="nav__menu">
      <div className="nav__products">
        <Button handleClick={handleSelectAll} title="Select ALL" />
        <p className="allProducts" id="totalProducts">{` seledcted ${
          idArray && idArray.length
        }  out of ${productsList && productsList.length}  products`}</p>
        <p className="allProducts_Length">{`${productsList && productsList.length}  products`}</p>
        {idArray.length > 0 && (
          <button
            idArray={idArray}
            onClick={handleClearAll}
            className="clear_all"
          >clear all</button>
        )}
      

        {user && user.isAdmin && (
          <>
            <input
              type="button"
              className="createButton"
              onClick={handleOpen}
              value="create product"
            />
            <AddBoxIcon
              className={classes.root}
              onClick={handleOpen}
            ></AddBoxIcon>
          </>
        )}
      </div>
      <div className="nav__search">
        <input
          placeholder="search..."
          className="nav__search-input"
          type="text"
          id="searchQuery"
          value={searchType}
          onChange={searchInputChanged}
          onKeyDown={handleEnter}
        />
        <button
          className="nav__search-btn"
          id="searchButton"
          onClick={handleSearchClick}
        >
          <SearchIcon style={{ color: "grey", marginTop: "5px" }} />
        </button>

        <img className="question" src={questionLogo} alt="" />
        <MenuIcon
          onClick={() => {
            setDrawerOpen(true);
          }}
          className={classes.root}
        />
        <Drawer
          open={drawerOpen}
          onClose={() => {
            setDrawerOpen(false);
          }}
          anchor="right"
        >
          <SidebarNav />
        </Drawer>
      </div>
    </div>
  );
};

export default NavMenu;
