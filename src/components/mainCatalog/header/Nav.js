import "./Nav.css";
import NavMenu from "./NavMenu";
import { useDispatch, useSelector } from "react-redux";
import { changeSortTypeAction } from "../../../Reducers/ProductsReducer/ProductsAction";
import { useEffect } from "react";
import { getProductsAction } from "../../../Reducers/ProductsReducer/ProductsAction";
import { sortProducts } from "../../../sortProducts";

const Nav = ({
  sortProducts,
  sortType,
  handleSearchClick,
  handleClearAll,
  handleSelectAll,
  idArray,
  categoryValue,
  setSearchInput,
  searchInput,
}) => {
  const dispach = useDispatch();
  const productsList = useSelector((state) => state.products.productsList);
  const sorttType = useSelector((state) => state.products.sortType);

  const handleSort = (event) => {
    dispach(changeSortTypeAction(event.target.value));
    sortProducts(event.target.value, productsList);
  };
  useEffect(() => {
    const sort = sortProducts(sorttType, productsList);
    dispach(getProductsAction(sort));
  }, [sorttType]);
  return (
    <nav className="nav">
      <NavMenu
        handleSearchClick={handleSearchClick}
        categoryValue={categoryValue}
        sortType={sortType}
        sortProducts={sortProducts}
        handleClearAll={handleClearAll}
        handleSelectAll={handleSelectAll}
        idArray={idArray}
        setSearchInput={setSearchInput}
        searchInput={searchInput}
      />
      <div className="nav__sort">
        <select id="sort" onChange={handleSort}>
          <option value="default">New Arrivals</option>
          <option value="asc">Price: Low to High</option>
          <option value="desc">Price: High to Low</option>
          <option value="alphabetic">Alphabet</option>
          <option value="reverse-alphabetic">Reverse Alphabet</option>
        </select>
      </div>
    </nav>
  );
};
export default Nav;
