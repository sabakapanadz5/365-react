import { Slider, makeStyles } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getProductsAction } from "../../Reducers/ProductsReducer/ProductsAction";
import "./SliderBar.css";

const useStyles = makeStyles(() => ({
  slider: {
    color: "#49547d",
  },
}));
const SliderBar = ({ setValue, title, min, max, value }) => {
  const classes = useStyles();
  const dispach = useDispatch();

  const sliderValue = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const products = JSON.parse(localStorage.getItem("products"));
    let filteredArrMinprice =
      products && products.filter((item) => item.price >= value[0]);
    let filteredArr =
      filteredArrMinprice &&
      filteredArrMinprice.filter((item) => item.price <= value[1]);
    dispach(getProductsAction(filteredArr));
  }, [value]);
  return (
    <div className="slider-wrapper">
      <div className="slider-fix">
        <span className="slider-title">{title}</span>
        <Slider
          value={value}
          min={parseInt(min)}
          max={parseInt(max)}
          className={classes.slider}
          valueLabelDisplay="auto"
          aria-labelledby="range-slider"
          onChange={sliderValue}
        ></Slider>
        <div className="slider__values">
          <div className="startingValue">
            <div>{"$"}</div>
            <div>{value[0]}</div>
          </div>
          <div className="endingValue">
            <div>{"%"}</div>
            <div>{value[1]}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SliderBar;
