import Catalog from "./catalog/Catalog";
import "./Main.css";
import Nav from "../mainCatalog/header/Nav";
import { useState, useEffect } from "react";
import Modal from "./catalog/Modal";
import axios from "axios";
import Filter from "./Filter";
import { useParams, useHistory } from "react-router";
import { CircularProgress } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {
  changeCategoryValueAction,
  getProductsAction,
} from "../../Reducers/ProductsReducer/ProductsAction";
import sortProducts from "../../sortProducts";
import { modalOpenAction } from "../../Reducers/ModalReducer/ModalAction";

const Main = () => {
  const { categoryUrl, id } = useParams();
  const dispach = useDispatch();
  const productsList = useSelector((state) => state.products.productsList);
  const sorttType = useSelector((state) => state.products.sortType);
  const modalDataa = useSelector((state) => state.modal.modalData);
  const categoryValue = useSelector((state) => state.products.categoryValue);

  const history = useHistory();
  const [idArray, setIdArray] = useState([]);
  // const [categoryValue, setcategoryValue] = useState("default");
  const [loading, setLoading] = useState(true);
  // const [modalData, setModalData] = useState(id);

  useEffect(() => {
    dispach(modalOpenAction(id));
  }, []);

  const handleOpen = (info) => {
    dispach(modalOpenAction(info));
    history.push(`/catalog/${categoryValue}/${info.id}`);
  };
  useEffect(() => {
    const item = productsList && productsList.find((item) => item.id == id);
    dispach(modalOpenAction(item));
  }, [productsList]);

  useEffect(() => {
    dispach(changeCategoryValueAction(categoryUrl || "default"));
  }, []);

  const handleClose = (event) => {
    if (event.target !== event.currentTarget) return;
    history.goBack();
    dispach(modalOpenAction());
  };
  const handleSelectAll = () => {
    setIdArray(productsList.map((item) => item.id));
  };
  const handleClearAll = () => {
    setIdArray([]);
  };
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) history.push("/signin");
  }, []);

  useEffect(() => {
    setLoading(true);
    if (categoryValue == "default") {
      dispach(getProductsAction(JSON.parse(localStorage.getItem("products"))));
    } else {
      dispach(
        getProductsAction(
          JSON.parse(localStorage.getItem("products")) &&
            JSON.parse(localStorage.getItem("products")).filter(
              (item) => item.category === categoryValue
            )
        )
      );
    }
    setLoading(false);
  }, [categoryValue]);

  useEffect(() => {
    setIdArray([]);
  }, [categoryValue]);

  return (
    <div className="main-container">
      {loading && JSON.parse(localStorage.getItem("products")) && (
        <CircularProgress className="circular"></CircularProgress>
      )}
      <main className="main">
        <div className="main-wrapper">
          <Filter />
        </div>
        <div className="catalog-wrapper">
          <Nav
            sortProducts={sortProducts}
            handleClearAll={handleClearAll}
            handleSelectAll={handleSelectAll}
            idArray={idArray}
          />
          <Catalog
            idArray={idArray}
            handleClose={handleClose}
            handleOpen={handleOpen}
            setIdArray={setIdArray}
          />
        </div>
        {modalDataa && <Modal handleClose={handleClose} />}
      </main>
    </div>
  );
};
export default Main;
