import { useEffect, useState } from "react";
import CatalogHover from "./CatalogHover";
const CatalogItem = ({
  idArray,
  val,
  id,
  description,
  handleOpen,
  image,
  title,
  price,
  setIdArray,
}) => {
  const [selected, setselected] = useState(false);
  const [reloader, setReloader] = useState(false);

  const Changed = (event) => {
    setselected(event.target.checked);

    setIdArray((prev) => {
      const foundId = prev.find((item) => item === id);
      if (foundId) {
        const newArr = prev.filter((item) => item !== id);
        return newArr;
      } else {
        return [...prev, id];
      }
    });
  };
  useEffect(() => {
    const isFound = idArray && idArray.find((item) => item == id);
    setselected(isFound);
  }, [idArray]);

  return (
    <div>
      {val === "catalog" && (
        <div
          className={`catalog__product ${
            selected ? `catalog__product--border` : ""
          }`}
        >
          {val === "catalog" && (
            <CatalogHover
              id={id}
              isClicked={selected}
              checkboxChange={Changed}
            />
          )}
          <div
            className="catalog__item"
            onClick={() => handleOpen({ id, image, price, title, description })}
          >
            <div className="catalog__photo">
              <img src={image} />
            </div>
            <div className="catalog__title">
              <h3 className="product__heading">{title}</h3>
            </div>
            <div className="product__item product__item--price">
              <div className="price__item">
                <div className="price">${price}</div>
                <div className="rrp">RRP</div>
              </div>
              <div className="divider"></div>
              <div className="price__item">
                <div className="price">$6</div>
                <div className="rrp">Cost</div>
              </div>
              <div className="divider"></div>
              <div className="price__item">
                <div className="price price__profit">60%/$9</div>
                <div className="rrp">Profit</div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default CatalogItem;
