import { Grid } from "@material-ui/core";
import "./Catalog.css";
import CatalogItem from "./CatalogItem";
import { useDispatch, useSelector } from "react-redux";
import { getProductsAction } from "../../../Reducers/ProductsReducer/ProductsAction";
import { useEffect } from "react";
import { getProducts } from "../../../API";
import EditAddProduct from "../../EditAddProduct/EditAddProduct";
const Catalog = ({ idArray, handleOpen, setIdArray }) => {
  const productsList = useSelector((state) => state.products.productsList);

  const dispach = useDispatch();

  useEffect(() => {
    getProducts().then((result) => {
      dispach(getProductsAction(result));
    });
  }, []);

  return (
    <>
    <EditAddProduct />
    <Grid container className="catalog">
      {productsList &&
        productsList.map((item) => (
          <Grid  key={item.id} item lg={3} md={4} sm={6} xl={3} xs={12}>
            <CatalogItem
              handleOpen={handleOpen}
              key={item.id}
              idArray={idArray}
              id={item.id}
              price={item.price}
              title={item.title}
              image={item.imageUrl}
              setIdArray={setIdArray}
              description={item.description}
              val="catalog"
            />
          </Grid>
        ))}
    </Grid>
    </>
  );
};
export default Catalog;
