import { addToCart } from "../../../API";
import Button from "../../Common/Button";
import "./Modal.css";
import { useSelector } from "react-redux";
import { useSnackbar } from 'notistack';

const Modal = ({ handleClose }) => {
  const modalDataa = useSelector((state) => state.modal.modalData);
  const { enqueueSnackbar } = useSnackbar();
  const addOneItem = async () => {
    await addToCart(modalDataa.id, 1);
    let variant ="success"
    enqueueSnackbar('item removed sucsusfully!', { variant });
  };
  return (
    <div className="modal__container" onClick={handleClose}>
      <div className="modal__card">
        <div className="modal__close" onClick={handleClose}>
          ✖
        </div>
        <div className="modal__item">
          <div className="modal-item__right">
          <div className="item__info item__info--price">
                            <div className="price__item"><span className="span__item--styled">${modalDataa.price}</span> <br/>RRP</div>
                            <div className="price__item"><span className="span__item--styled">$6</span>  <br/>Cost</div>
                            <div className="price__item"><span className="span__item--styled span__item--blue">60% / 9$</span>  <br/>Profit</div>
                        </div>

            <div className="item__info-image">
              <img
                className="item__image"
                alt="product"
                src={modalDataa.image}
              />
            </div>
          </div>
          <div className="modal-item__left">
            <h1 className="item_title">{modalDataa.title}</h1>
            <Button
              handleClick={addOneItem}
              title="Add to My Inventory"
              big
            />
            <h3 className="item_productdesc">Product Description</h3>
            <div className="item_description">{modalDataa.description}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Modal;
