import Button from "../../Common/Button";
import "./CatalogHover.css";
import Checkbox from "../../Common/Checkbox";
import { addToCart, deleteProduct, getCart, getProducts } from "../../../API";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { useSnackbar } from 'notistack';
import {
  getProductsAction,
} from "../../../Reducers/ProductsReducer/ProductsAction";
import { editModalOpenAction } from "../../../Reducers/ModalReducer/ModalAction";

const CatalogHover = ({ id, checkboxChange, isClicked }) => {
  const user = JSON.parse(localStorage.getItem("user")) || {}
  const { enqueueSnackbar } = useSnackbar();
  const dispach = useDispatch();
  const addCart = async () => {
    addToCart(id, 1)
      .then(() =>{
        let variant ="success"
        enqueueSnackbar('product added!', { variant });
      })
      .catch((err) => {
        let variant ="error"
        enqueueSnackbar('product add failed', { variant });
      });
  };
  const editClick = () => {
    // history.push(`/product/${id}`);
    dispach(editModalOpenAction(id))

  };
  const deleteprod = async () => {
    await deleteProduct(id);
    await getProducts()
      .then((result) => {
        dispach(getProductsAction(result));
      }).then(() =>{
        let variant ="success"
        enqueueSnackbar('product deleted successfully', { variant });
      })
      .catch((err) => {
        let variant ="error"
        enqueueSnackbar('delete product failed', { variant });
      });
  };

  return (
    <div className={`Catalog__Hover`}>
      <div
        className={`catalog__hover-checkbox ${
          isClicked && "catalog__hover--clicked"
        }`}
      >
        <Checkbox checkboxChange={checkboxChange} isClicked={isClicked} />
      </div>
      <div className="catalog__hover-button">
        {user && user.isAdmin && (
          <>
            <EditIcon style={{ color: "#61d5df" }} onClick={editClick} />
            <DeleteIcon style={{ color: "#61d5df" }} onClick={deleteprod} />
          </>
        )}

        <Button addprod handleClick={addCart} title="Add to Inventory" />
      </div>
    </div>
  );
};
export default CatalogHover;
