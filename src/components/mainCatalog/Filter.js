import Sidebar from "./Sidebar";
import axios from "axios";
import { useState, useEffect } from "react";
import ShipOptions from "./ShipOption";
import { Slider } from "@material-ui/core";
import SliderBar from "./SliderBar";
import Button from "../Common/Button";

const Filter = ({ categoryValue, setcategoryValue }) => {
  const [categories, setCategories] = useState([]);
  const [priceRange, setPriceRange] = useState([0, 1000]);

  useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories").then((res) => {
      setCategories(res.data);
    });
  }, []);
  const resetFilter = () => {
    setPriceRange([0, 1000]);
  };
  return (
    <div className="sidebar__menu">
      <Sidebar title="Choose Niche" />
      <Sidebar
        categoryValue={categoryValue}
        setcategoryValue={setcategoryValue}
        categories={categories}
        title="Choose Category"
        light
      />
      <ShipOptions title="Ship From" modifier="shipfrom" />
      <ShipOptions title="Ship To" modifier="shipto" />
      <ShipOptions title="Select Supplier" modifier="supplier" />
      <SliderBar
        value={priceRange}
        setValue={setPriceRange}
        min="0"
        max="10000"
        title="price range"
      />
      <div className="button-position">
        <Button slider handleClick={resetFilter} title="reset filter" />
      </div>
    </div>
  );
};
export default Filter;
