import { getCart } from "../../API";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCartProductsAction } from "../../Reducers/ProductsReducer/ProductsAction";
import CartItem from "./CartItem";
import "./CartItem.css";
import { makeStyles } from "@material-ui/core";
import { Drawer } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import SidebarNav from "../sidebar/SidebarNav";

const styles = makeStyles((theme) => ({
  root: {
    cursor: "pointer",
    color: "#61d5df",
    fontSize: "xxx-large",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

const Cart = () => {
  const classes = styles();
  const dispach = useDispatch();
  const cartProducts = useSelector((state) => state.products.cartProducts);
  const [drawerOpen, setDrawerOpen] = useState(false);
  useEffect(() => {
    getCart().then((result) => {
      dispach(getCartProductsAction(result));
    });
  }, []);

  return (
    <div className="cart">
      <div className="cart__total">
        Cart total:{" "}
        <span className="cart__total--span">
          ${cartProducts && cartProducts.cartItem && cartProducts.cartItem.totalAmount}
        </span>
        <div className="burger">
          <MenuIcon
            onClick={() => {
              setDrawerOpen(true);
            }}
            className={classes.root}
          />
          <Drawer
            open={drawerOpen}
            onClose={() => {
              setDrawerOpen(false);
            }}
            anchor="right"
          >
            <SidebarNav />
          </Drawer>
        </div>
      </div>

      <div className="cart__content">
        <table className="cart__table">
          <tr className="cart__row cart__row--header">
            <th className="cart__header cart__header--img">Product image</th>
            <th className="cart__header">Title</th>
            <th className="cart__header cart__header--quantity">Quantity</th>
            <th className="cart__header">Remove/Add </th>
            <th className="cart__header">Remove all </th>
            <th className="cart__header">Total</th>
          </tr>
          {cartProducts && cartProducts.cartItem &&
            cartProducts.cartItem.items.map((item) => {
              return (
                <CartItem
                  item={item}
                  qty={item.qty}
                  key={item.id}
                  id={item.id}
                  price={item.price}
                  title={item.title}
                  image={item.image}
                />
              );
            })}
        </table>
      </div>
    </div>
  );
};
export default Cart;
