import "./CartItem.css";
import Button from "../Common/Button";
import { addToCart, getCart, removeFromCart } from "../../API";
import { useDispatch } from "react-redux";
import { getCartProductsAction } from "../../Reducers/ProductsReducer/ProductsAction";
import { useEffect } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { useSnackbar } from 'notistack';


const CartItem = ({ title, price, image, qty, id }) => {
  const dispach = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const removeOneItem = async () => {
    await addToCart(id, -1);
    const res = await getCart()
    dispach(getCartProductsAction(res))
    let variant ="success"
    enqueueSnackbar('item removed sucsusfully!', { variant });
   
   
    
  };
  useEffect(() => {
    if (qty === 0) {
      ZeroItem();
    }
  }, [qty]);

  const addOneItem = async () => {
    await addToCart(id, 1);
    let res = await getCart()
    dispach(getCartProductsAction(res));
    let variant ="success"
    enqueueSnackbar('item added sucsusfully!', { variant });
  };

  const removeItem = async () => {
    try{
    await removeFromCart(id);
    let res = await getCart()
    dispach(getCartProductsAction(res));
    let variant ="success"
    enqueueSnackbar('item removed sucsusfully!', { variant });
  }catch{
    let variant ="error"
    enqueueSnackbar('try again!', { variant });
  }

    
  };

  const ZeroItem = async () => {
    try{
    await removeFromCart(id);
    let res = await getCart()
      dispach(getCartProductsAction(res));
    }catch{
     console.log("err")
    }
   
  };

  return (
    <tr className="cart__row">
      <td className="cart__data cart__data--img">
        <img alt="cart product" src={image} className="cart__img" />
      </td>
      <td className="cart__data cart__data--title">{title}</td>
      <td className="cart__data cart__data--quantity"> {qty} x</td>
      <td className="cart__data cart__data--remove-all">
        <div className="cart_plus">
          <Button plus handleClick={removeOneItem} title="-" />
          <Button plus handleClick={addOneItem} title="+" />
        </div>
      </td>
      <td className="cart__data cart__data--removeAll">
        <DeleteIcon
          className="deleticon"
          style={{ color: "#61d5df", fontSize: "xx-large" }}
          onClick={removeItem}
        />
      </td>
      <td className="cart__data cart__data--total">{qty * price}$</td>
    </tr>
  );
};
export default CartItem;
