import "./Button.css";
const Button = ({ big, handleClick, title, slider, signUp, addprod, plus }) => {
  return (
    <div>
      <button
        onClick={handleClick}
        className={
          `nav__btn` +
          (big ? ` nav__btn--big` : ``) +
          (slider ? ` nav__btn--slider` : ``) +
          (signUp ? ` nav__btn--signUp` : ``) +
          (addprod ? ` nav__btn--addprod` : ``) +
          (plus ? ` nav__btn--plus` : ``)
        }
      >
        {title}
      </button>
    </div>
  );
};
export default Button;
