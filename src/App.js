import "./App.css";
import Aside from "./components/sidebar/Aside";
import Main from "./components/mainCatalog/Main";
import Landing from "./components/landingPage/Landing";
import { Route, Switch, useLocation } from "react-router-dom";
import SingUp from "./components/signUp/SignUp";
import SingIn from "./components/SignIn/SignIn";
import { useState, useEffect } from "react";
import Cart from "./components/Cart/Cart";
import Profile from "./components/Profile/Profile";
import EditAddProduct from "./components/EditAddProduct/EditAddProduct";
function App() {
  const [renderAside, setRenderAside] = useState(false);
  const location = useLocation();
  useEffect(() => {
    if (
      location.pathname === "/" ||
      location.pathname === "/signup" ||
      location.pathname === "/signin"
    ) {
      setRenderAside(false);
    } else {
      setRenderAside(true);
    }
  }, [renderAside, location]);
  return (
    <div className="conteiner">
      {renderAside && <Aside />}
      <Switch>
        <Route path="/dropship">
          <p>comming soon dropship😉</p>
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/orders">
          <p>comming soon orders😉</p>
        </Route>
        <Route path="/transactions">
          <p>comming soon transactions😉</p>
        </Route>
        <Route path="/dashboard">
          <p>comming soon dashboard😉</p>
        </Route>
        <Route path="/inventory">
          <p>comming soon inventory😉</p>
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
        <Route exact path="/catalog/:categoryUrl?/:id?">
          <Main />
        </Route>
        <Route exact path="/">
          <Landing />
        </Route>
        <Route path="/signup">
          <SingUp />
        </Route>
        <Route path="/signin">
          <SingIn />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
