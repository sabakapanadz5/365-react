import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import "@fontsource/roboto";
import { combineReducers, createStore } from "redux";
import { Provider } from "react-redux";
import productsReducer from "./Reducers/ProductsReducer/ProductsReducer";
import modalReducer from "./Reducers/ModalReducer/ModalReducer";
import { SnackbarProvider} from 'notistack';

const reducers = combineReducers({
  products: productsReducer,
  modal: modalReducer,
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
    <SnackbarProvider maxSnack={5}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
      </SnackbarProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
