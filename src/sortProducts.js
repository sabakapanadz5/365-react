const sortProducts = (value, newArr) => {
  if (value == "asc") {
    newArr.sort((a, b) => a.price - b.price);
  } else if (value == "desc") {
    newArr.sort((a, b) => b.price - a.price);
  } else if (value == "alphabetic") {
    newArr.sort((a, b) => a.title.localeCompare(b.title));
  } else if (value == "reverse-alphabetic") {
    newArr.sort((a, b) => b.title.localeCompare(a.title));
  } else {
    newArr.sort((a, b) => a.id - b.id);
  }

  return newArr;
};

export default sortProducts;
